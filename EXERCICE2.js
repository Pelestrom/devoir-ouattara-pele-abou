function isPremier(nombre) {
    if (nombre <= 1) {
      return false;
    }
  
    if (nombre <= 3) {
      return true;
    }
  
    if (nombre % 2 === 0 || nombre % 3 === 0)     // Vérifier la divisibilité par 2 ou 3
    {
      return false;
    }

    // Vérifier la divisibilité par les nombres de la forme 6k ± 1
    for (let i = 5; i * i <= nombre; i += 6) 
    {
      if (nombre % i === 0 || nombre % (i + 2) === 0)
      {
        return false;
      }
    }
       return true;
  }
  
  // Tester la fonction avec les exemples donnés
  console.log("0", isPremier(0)); // false
  console.log("1", isPremier(1)); // false
  console.log("2", isPremier(2)); // true
  console.log("3", isPremier(3)); // true
  console.log("11", isPremier(11)); // true
  console.log("12", isPremier(12)); // false
  