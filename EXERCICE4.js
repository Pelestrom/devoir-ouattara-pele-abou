function demanderMoy()  // Fonction pour demander une Moy à l'utilisateur
  {
    return parseFloat(prompt("Veuillez entrer votre moyenne (entre 0 et 20 SVP) :) "));
  }
  
  function afficherMention(Moy)    // Fonction pour afficher la mention correspondante
   {
    if (Moy >= 0 && Moy <= 20) {
      if (Moy >= 18) {
        console.log("Excellent");
      } else if (Moy >= 16) {
        console.log("Très bien");
      } else if (Moy >= 14) {
        console.log("Bien");
      } else if (Moy >= 12) {
        console.log("Assez Bien");
      } else if (Moy >= 10) {
        console.log("Passable");
      } else if (Moy >= 8) {
        console.log("Insuffisant");
      } else if (Moy >= 6) {
        console.log("Faible");
      } else {
        console.log("Médiocre");
      }
    } else {
      console.log("ATTENTION!!! :(  Veuillez entrer une moyenne valide entre 0 et 20.");
    }
  }
  const MoyUser = demanderMoy(); // Demander la Moy à l'utilisateur

  afficherMention(MoyUser);   // Afficher la mention correspondante
  