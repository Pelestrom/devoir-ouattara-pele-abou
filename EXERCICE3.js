class Rectangle {
    constructor(largeur, hauteur) {
      this.width = largeur;
      this.height = hauteur;
    }

    get perimeter() // Méthode pour calculer le périmètre
    {
      return 2 * (this.width + this.height);
    }

    get isValid()  // Propriété pour vérifier si le rectangle est valide
     {
      return this.width > 0 && this.height > 0;
     }
  
    isBiggerThan(otherRectangle) // Méthode pour comparer la taille avec un autre rectangle
     {
      return this.width * this.height > otherRectangle.width * otherRectangle.height;
     }
  }
  
  class Square extends Rectangle {
    constructor(cote)
    {
      super(cote, cote);  // Un carré a la même dimension pour la largeur et la hauteur
    }
  }
  
  // Tester les classes avec les exemples donnés
  const rectangle1 = new Rectangle(10, 20);
  console.log(rectangle1.perimeter); // 60
  console.log(rectangle1.isValid); // true
  
  const rectangle2 = new Rectangle(-10, 20);
  console.log(rectangle2.isValid); // false
  
  const carre1 = new Square(10);
  console.log(carre1.perimeter); // 40
  console.log(rectangle1.isBiggerThan(carre1)); // true
  