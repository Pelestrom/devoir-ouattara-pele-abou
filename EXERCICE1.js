function demanderNombre() // Fonction pour demander un nombrUserNombre
  {
    return parseInt(prompt("Entrer un nombre entre 0 et 10 SVP :) "));
  } 

  function afficherNombres(n)   // Fonction pour afficher les nombres inférieurs ou égaux au nombre donné
  {
    for (let i = n; i >= 0; i--) 
    {
      console.log(i);
    }
  }
  
  // Boucle pour gérer la demande jusqu'à ce l'Utilisateur entre un nombre valide
  let UserNombre;
  
  do {
    UserNombre = demanderNombre();
  
    if (UserNombre < 0 || UserNombre > 10 || isNaN(UserNombre)) {
      alert("ATTENTION!!! :(  Veuillez entrer un nombre valide entre 0 et 10 SVP.");
    }
  
  } while (UserNombre < 0 || UserNombre > 10 || isNaN(UserNombre));
  
  afficherNombres(UserNombre);   // Afficher les nombres
  